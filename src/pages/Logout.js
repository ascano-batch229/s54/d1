import{ useContext, useEffect} from 'react';
import{ Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout(){

	// Consume the UserContext object and destructure it to access the userstate and unsetUsert function from the Context provider
	const {unsetUser, setUser} = useContext(UserContext);

	// Clear the localStorage of user's information
	unsetUser();

	// localStorage.clear();

	useEffect(() => {

		// Set the user state back to its original value
		setUser({ email:null })

	})


	return(
			<Navigate to="/login"/>
		)

}